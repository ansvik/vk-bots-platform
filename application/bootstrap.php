<?php

require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/queue.php';
require_once 'core/consts.php';
require_once 'parsers/aiml_parser.php';
require_once 'parsers/ini_parser.php';

require_once 'core/route.php';
Route::start();
