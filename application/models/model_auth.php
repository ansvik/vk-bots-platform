<?php

class Model_Auth extends Model
{
	
	function __construct()
	{
		$this->client_id = CLIENT_ID;                           
		$this->client_secret = CLIENT_SECRET;                   
		$this->redirect_uri = '';               // url страницы авторизации
	}
	
	public function get_data()
	{	
		$url = 'http://oauth.vk.com/authorize';

        $params = array(
            'client_id'     => $this->client_id,
            'scope' => 'groups, offline',
            'redirect_uri'  => $this->redirect_uri,
            'response_type' => 'code',
            'v' => '5.74'
        );
		
		return $url . '?' . http_build_query($params);
	}

    public function login($code) {
            $result = false;
            $params = array(
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'code' => $code,
                'redirect_uri' => $this->redirect_uri,
                'v' => '5.74'
            );

            $token = file_get_contents('https://oauth.vk.com/access_token' . '?' . http_build_query($params));
            $token = json_decode($token, true);
            print_r($token);
            if (isset($token['access_token'])) {
                $params = array(
                    'uids'         => $token['user_id'],
                    'fields'       => 'uid,first_name,last_name,sex,photo_big',
                    'access_token' => $token['access_token'],
                    'v' => '5.74'
                );

                $userInfo = file_get_contents('https://api.vk.com/method/users.get' . '?' . http_build_query($params));
        
                $userInfo = json_decode($userInfo, true);
        
                if (isset($userInfo['response'][0]['id'])) {
                    $userInfo = $userInfo['response'][0];
                    $result = true;
                }
            }

            if ($result) {
                if (!is_dir(WEBSITE_PATH . '/application/users/' . $userInfo['id'])){
                    mkdir(WEBSITE_PATH . '/application/users/' . $userInfo['id']);
                }
                
                $_SESSION['user'] = $userInfo;
                $_SESSION['token'] = $token;
                if (file_exists(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] .'/settings.ini')){
                    $ini = parse_ini_file(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] .'/settings.ini');
                    if (isset($ini['mainbot']))
                        $_SESSION['mainbot'] = $ini['mainbot'];
                    else $_SESSION['mainbot'] = -1;
                }
                else {
                    $_SESSION['mainbot'] = -1;
                }
            }
    }
}
?>