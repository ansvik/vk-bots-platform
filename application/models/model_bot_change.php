<?php

class Model_Bot_change extends Model
{
    
    function get_data()
    {
        $str;
        $user = $_SESSION['user'];
        $dir = scandir(WEBSITE_PATH . 'application/users/' . $user['id']);
        $dir = array_diff($dir, array('..', '.'));
        $str .= '<div id="bots">';
        foreach ($dir as $file) {
            if (stripos($file, '.aiml')){
                $name = explode('.', $file);
                $str .= '<div class="bot_block">';
                $str .= $name[0];
                $str .= '<br>';
                if ($file !== $_SESSION['mainbot']) $str .= "<button type='button' onclick='setmainbot(\"" . $file. "\")'>Сделать текущим</button>";
                $str .= "<button type='button' onclick='deletebot(\"" . $file . "\")'>Удалить</button>";
                $str .= '</div>';
            }
        }
        $str .= '</div>';
        return $str;
    }
    
}