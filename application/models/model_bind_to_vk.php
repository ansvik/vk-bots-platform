<?php

class Model_Bind_to_vk extends Model
{

    function __construct()
    {
        $this->client_id = CLIENT_ID;
        $this->client_secret = CLIENT_SECRET;
        $this->redirect_uri = ''; // Адрес сайта + /bind_to_vk/get_code
    }
    
    function get_data()
    {
        $x;

        $params = array(
            'user_id' => $_SESSION['user']['id'],
            'extended' => 1,
            'filter' => 'admin',
            'access_token' => $_SESSION['token']['access_token'],
            'v' => '5.74'
        );

        $params = http_build_query($params);

        $groups = file_get_contents('https://api.vk.com/method/groups.get?' . $params);
        $groups = json_decode($groups, true);
        $groups = $groups['response'];

        if ($groups['count'] == 0)
        {
            return 'У вас нет администрируемых групп.';
        }
        
        $dir = scandir(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id']);
        $dir = array_diff($dir, array('..', '.'));
        
        $binded_groups = new Queue();
        
        foreach ($dir as $file) 
        {
            $name = explode('.', $file);
            
            if($name[1] == 'php')
            {
                $binded_groups->push($name[0]);
            }
        }
        
        $binded_groups = $binded_groups->getArray();

        $url = 'http://oauth.vk.com/authorize?';
        $unbind_url = 'bind_to_vk/unbind';

        $params = array(
            'client_id'     => $this->client_id,
            'redirect_uri'  => $this->redirect_uri,
            'scope' => 'manage, messages',
            'response_type' => 'code',
            'v' => '5.74'
        );

        $x .= '<div class="groups">';

        foreach($groups['items'] as $item)
        {
            if (in_array($item['id'], $binded_groups))
            {
                continue;
            }
            $x .= '<form action="bind_to_vk/check_token" method="get">';
            $x .= $item['name'] . ' ';
            $x .= '<input type="hidden" value="' . $item['id'] . '" name="group">';
            $x .= '<input type="hidden" value="' . $url . http_build_query($params) . '&group_ids=' . $item['id'] . '" name="link">';
            $x .= '<button type="submit">' . Привязать . '</button>';
            $x .= '</form>';
        }

        $x .= '</div>';
        $x .= '<div class="groups">';
        
        foreach($groups['items'] as $item)
        {
            if (!in_array($item['id'], $binded_groups))
            {
                continue;
            }
            $x .= '<form class="groups" action="' . $unbind_url . '" method="get">';
            $x .= $item['name'] . ' ';
            $x .= '<input type="hidden" value="' . $item['id'] . '" name="group">';
            $x .= '<button type="submit">' . Отвязать . '</button>';
            $x .= '</form>';
        }

        $x .= '</div>';
        
        return $x;
    }
    
    function get_code($code)
    {
        
        $group_id = $_SESSION['group'];
        $result = false;
        $params = array(
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'code' => $code,
            'redirect_uri' => $this->redirect_uri,
            'v' => '5.74'
        );
    
        $token = file_get_contents('https://oauth.vk.com/access_token' . '?' . http_build_query($params));
        $token = json_decode($token, true);
    
        if (isset($token['access_token_' . $group_id])) {
            
            $ini = parse_ini_file(get_settings_path());
            $ini[$group_id] = $token['access_token_' . $group_id];
            ini_write($ini, get_settings_path());
            
            $this->bind($token['access_token_' . $group_id], $group_id);
        }
        
    }
    
    function bind($token, $group_id)
    {
        if ($this->check_servers($token, $group_id)) {
        
            $php_file_name = array_diff($_SESSION['mainbot'], '.aiml');
        
            //$url = 'application/users/' . $_SESSION['user']['id'] . '/' . $group_id . $php_file_name . '.php';
            
            $url = 'bot?id=' . $_SESSION['user']['id'];
            
            $params = array(
                'group_id' => $group_id,
                'access_token' => $token,
                'v' => '5.80'
            );
            
            $x = file_get_contents('https://api.vk.com/method/groups.getCallbackConfirmationCode' . '?' . http_build_query($params));
            $x = json_decode($x, true);
            
            $ctoken = $x['response']['code'];
            
            $secret = openssl_random_pseudo_bytes(10);
            $secret = bin2hex($secret);
            
            $bot = file_get_contents(WEBSITE_PATH . '/application/users/template.php');
            $bot = preg_replace('/\{token\}/', $token, $bot);
            $bot = preg_replace('/\{ctoken\}/', $ctoken, $bot);
            $bot = preg_replace('/\{skey\}/', $secret, $bot);
            $bot = preg_replace('/\{file\}/', WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' . $_SESSION['mainbot'], $bot);
            $bot = preg_replace('/\{join\}/', 'Welcome', $bot);
            $bot = preg_replace('/\{leave\}/', 'Goodbye', $bot);
            
            $file_handle = fopen(WEBSITE_PATH . '/application/users/'  . $_SESSION['user']['id'] . '/' . $group_id . $php_file_name . '.php', "w");
            fwrite($file_handle, $bot); 
            fclose($file_handle);
            
            $params = array(
                'group_id' => $group_id,
                'url' => $url,
                'title' => 'Chatbot',
                'secret_key' => $secret,
                'access_token' => $token,
                'v' => '5.80'
            );
            $x = file_get_contents('https://api.vk.com/method/groups.addCallbackServer' . '?' . http_build_query($params));
            $x = json_decode($x, true);
            
            $params = array(
                'group_id' => $group_id,
                'server_id' => $x['response']['server_id'],
                'api_version' => '5.73',
                'message_new' => '1',
                'group_join' => '1',
                'group_leave' => '1',
                'access_token' => $token,
                'v' => '5.80'
            );
            
            $x = file_get_contents('https://api.vk.com/method/groups.setCallbackSettings' . '?' . http_build_query($params));
        
            header('Location: ok');
            exit();
        }
        else
        {
            header('Location: error?num=1');
            exit();
        }
    }
    
    function check_servers($token, $group_id) {
        $params = array(
            'group_id' => $group_id,
            'access_token' => $token,
            'v' => '5.80'
        );
        
        $x = file_get_contents('https://api.vk.com/method/groups.getCallbackServers' . '?' . http_build_query($params));
        $servers = json_decode($x, true);
        
        foreach($servers['response']['items'] as $num => $val) {
            if ($val['title'] == 'Chatbot') {
                return false;
            }
        }
        return true;
    }
    
}   