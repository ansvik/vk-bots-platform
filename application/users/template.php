<?php

$confirmationToken = '{ctoken}';
$token = '{token}';
$secretKey = '{skey}';
$file = '{file}';

if(strcmp($data->secret, $secretKey) !== 0 && strcmp($data->type, 'confirmation') !== 0)
    return;

switch ($data->type) {
    case 'confirmation':
        echo $confirmationToken;
        break;

    case 'message_new':
        $userId = $data->object->user_id;
        $userInfo = json_decode(file_get_contents("https://api.vk.com/method/users.get?user_ids={$userId}&fields=sex&access_token={$token}&v=5.80"));
        $user_name = $userInfo->response[0]->first_name;
        $text = $data->object->body;
        $sex = $userInfo->response[0]->sex;

        $botsay = parse($file, $text, $userInfo);

        $request_params = array(
            'message' => $botsay,
            'user_id' => $userId,
            'access_token' => $token,
            'v' => '5.73'
        );
        $get_params = http_build_query($request_params);
        file_get_contents('https://api.vk.com/method/messages.send?' . $get_params);

        echo('ok');

        break;

    case 'group_join':
        $userId = $data->object->user_id;

        $request_params = array(
            'message' => "{join}",
            'user_id' => $userId,
            'access_token' => $token,
            'v' => '5.73'
        );

        $get_params = http_build_query($request_params);
        file_get_contents('https://api.vk.com/method/messages.send?' . $get_params);

        echo('ok');

        break;
    case 'group_leave':
        $userId = $data->object->user_id;

        $request_params = array(
            'message' => "{leave}",
            'user_id' => $userId,
            'access_token' => $token,
            'v' => '5.73'
        );

        $get_params = http_build_query($request_params);
        file_get_contents('https://api.vk.com/method/messages.send?' . $get_params);

        echo('ok');

        break;
}
?>