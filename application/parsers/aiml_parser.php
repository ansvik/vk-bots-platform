<?php

function parse($file, $str, $data) {
	$aiml = new SimpleXMLElement(file_get_contents($file));
	$name = (string) $aiml->bot['name'] . "\n";
	foreach ($aiml->category as $category) {
		foreach ($category->pattern as $pattern) {
			$reg = str_replace('*', '.*', $pattern);
			if (preg_match('/.*' . $reg . '.*/iu', $str)) {
				if (isset($category->template->random)){
					$size = count($category->template->random->li);
					$answer = replaceData($category->template->random->li[rand(0, $size - 1)], $name, $data);
					return $answer;	
				}
				else {
				    $answer = replaceData($category->template, $name, $datas);
					return $answer;
				}
			}
		}
	}
}

function replaceData($str, $name, $data = null) {
    $answer = str_replace("{name}", $name, $str);
	if ($data !== null) {
	    if ($data->response[0]->sex == 1) {
	        $answer = str_replace("[", '', $answer);
	        $answer = preg_replace('/\|.*\]/u', '', $answer);
	    }
	    else if ($data->response[0]->sex == 0 || $data->response[0]->sex == 2) {
	        $answer = str_replace("]", '', $answer);
	        $answer = preg_replace('/\[.*\|/u', '', $answer);
	    }
	}
	return $answer;
}

function aimlToHtml($file) {
	$aiml = new SimpleXMLElement(file_get_contents($file));
	$i = 0;
	$end = false;
	foreach ($aiml->category as $category) {
	    if ($category->pattern[0] == '*') {
	        echo '<p>Стандартный ответ: </p>';
	        echo '<input type="text" id="sanswer" value="';
	        if (isset($category->template->random)) echo $category->template->random->li;
	        else echo  $category->template;
	        echo '" />';
	        return;
	    }
		echo "<form>\n";
		echo "<input type=\"text\" class=\"in\" value=\"";
		foreach ($category->pattern as $pattern) {
			echo $pattern . "; ";
		}
		echo "\" ";
		echo "/>";
		echo "<input type=\"text\" class=\"out\" value=\"";
		foreach ($category->template as $template) {
			if (isset($template->random)){
					$size = count($template->random->li);
					for ($i2 = 0; $i2 < $size; $i2++)
						echo $template->random->li[$i2] . "; ";
				}
			else{
				echo $template;
			}
		}
		echo "\" />\n";
		echo "<button type=\"button\" onclick=\"deleteCategory(" . $i . ")\">delete</button> <br />";
		echo "</form>\n";
		$i++;
	}
}