<?php
    function arr2ini(array $a, array $parent = array())
    {
        $out = '';
        foreach ($a as $k => $v)
        {
            if (is_array($v))
            {
                $sec = array_merge((array) $parent, (array) $k);
                $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
                $out .= arr2ini($v, $sec);
            }
            else
            {
                $out .= "$k=$v" . PHP_EOL;
            }
        }
        return $out;
    }
    
    function ini_write(array $conf, $file){
        $inisave = arr2ini($conf);
        $file_handle = fopen($file, "w");
        fwrite($file_handle, $inisave); 
        fclose($file_handle);
    }
    
    function get_settings_path(){
        return WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/settings.ini';
    }
    
?>