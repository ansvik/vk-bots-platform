<script type="text/javascript">
            function setmainbot(file){
                var xhr = new XMLHttpRequest();			
			    xhr.open('POST', 'http://a0216170.xsph.ru/bot_change/set_main');
			    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			    xhr.send('bot=' + file);
			    location.reload();
            }
            function deletebot(file){
                var xhr = new XMLHttpRequest();			
			    xhr.open('POST', 'http://a0216170.xsph.ru/bot_change/delete');
			    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');			
			    xhr.onload = function (){
                	location.reload();
         	    }
			    xhr.send('bot=' + file);
            }
            function addbot(){
                var name = prompt('Введите имя.');
                var regex = /[^A-Za-z0-9]/u;
                if (name && regex.exec(name) == null) {
                    var xhr = new XMLHttpRequest();			
			        xhr.open('POST', 'http://a0216170.xsph.ru/bot_change/add');
			        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');			
			        xhr.onload = function (){
                    	location.reload();
                    }
			        xhr.send('bot=' + name);
                }
                else if (name == '') {
                    alert('Имя бота не может быть пустым!');
                }
                else if (regex.exec(name)) {
                    alert('Имя бота может содержать только латинские буквы и цифры.');
                }
            }
        </script>
        <?php
            echo $data;
        ?>
<br><br>
<button type='button' onclick='addbot()'>Добавить бота</button>