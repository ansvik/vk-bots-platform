<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<script type="text/javascript" src="/js/notify.js"></script>
	<title>Ansbots</title>
</head>
<body>
    <div id='notify'></div>
	<div id="header">
		<div id="logout"><a href="logout">Выход</a></div>
		<div id="logo"><a href="index">VkBots</a></div>
	</div>
	<div id="main">
		<div id="sidebar">
			<ul>
				<a href="conversation"><li>Разговор</li></a>
				<a href="bot_edit"><li>Редактирование бота</li></a>
				<a href="bot_change"><li>Выбор бота</li></a>
				<a href="bind_to_vk"><li>Привязка к ВК</li></a>
				<a href="help"><li>Помощь</li></a>
			</ul>
		</div>
		<div id="content">
			<?php include 'application/views/'.$content_view; ?>
		</div>
	</div>
	<div id="footer"></div>
</body>
</html>