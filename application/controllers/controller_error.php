<?php

class Controller_Error extends Controller
{
    
    function action_index()
    {
        if (isset($_GET['num']))
        {
            switch ($_GET['num']) 
            {
                case '1':
                    $data = 'К этой группе уже привязан бот.';
                    break;
                case '2':
                    $data = 'Не выбран основной бот. Пожалуйста <a href="http://a0216170.xsph.ru/bot_change">выберите</a> его.';
                    break;
                default: 
                    $data = 'Произошла ошибка.';
                    break;
            }
        }
        $this->view->generate('error_view.php', 'template_view.php', $data);
    }
    
}