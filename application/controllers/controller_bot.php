<?php

class Controller_Bot extends Controller
{
    
    function action_index()
    {
        
        if (!isset($_REQUEST)) {
            return;
        }
        
        $data = json_decode(file_get_contents('php://input'));
        
        require(WEBSITE_PATH . '/application/users/' . $_GET['id'] . '/' . $data->group_id . '.php');
    }
    
}