<?php

class Controller_Ok extends Controller
{
    
    function action_index()
    {
        $code = $_GET['code'];
        
        switch ($code)
        {
            case 1:
                $data = 'Бот успешно привязан.';
                break;
            case 2:
                $data = 'Бот успешно отвязан.';
                break;
            default:
                $data = 'Операция прошла успешно.';
                break;
        }
        
        $this->view->generate('ok_view.php', 'template_view.php', $data);
    }
    
}