<?php

class Controller_Auth extends Controller
{
	
	function __construct()
	{
		$this->model = new Model_Auth();
		$this->view = new View();
	}
	
	function action_index()
	{
	    if (isset($_GET['code'])) {
	        $this->model->login($_GET['code']);    
	        header('Location: index');
	        exit();
	    }
	    $data = $this->model->get_data();
		$this->view->generate('auth_view.php', 'empty_view.php', $data);
	}
	
}