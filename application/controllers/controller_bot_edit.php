<?php

class Controller_Bot_edit extends Controller
{
	
	function __construct()
	{
		$this->model = new Model_Bot_edit();
		$this->view = new View();
	}
	
	function action_index()
	{
	    if ($_SESSION['mainbot'] == -1)
        {
            header('Location: error?num=2');
        }
	    $data = $this->model->get_data(WEBSITE_PATH . '/application/users/' . '/' . $_SESSION['user']['id'] . '/' . $_SESSION['mainbot']);
		$this->view->generate('bot_edit_view.php', 'template_view.php', $data);
	}
	
	function action_create_aiml()
	{
	    $param = json_decode($_REQUEST['param'], true);
	    $outFile = fopen(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' .$_SESSION['mainbot'],"w");
	
	    fwrite($outFile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	    fwrite($outFile, "<aiml version=\"1.0\">\n");
    
	    for ($i = 0; $i < $param['size']; $i++){
	    	$keywords = $param['in'][$i];
	    	$keywords = explode("; ", $keywords);

	    	$answer = $param['out'][$i];
	    	$answer = explode("; ", $answer);
    
	    	fwrite($outFile, "<category>\n");

	    	foreach ($keywords as $key) {
		    	if (trim($key) !== '') fwrite($outFile, '<pattern>' . trim($key) . '</pattern>' . "\n");
		    }
		    fwrite($outFile, "<template>\n<random>\n");
		    foreach ($answer as $key) {
			    if (trim($key) !== '')  fwrite($outFile, '<li>' . trim($key) . '</li>' . "\n");
		    }
		    fwrite($outFile, "</random>\n</template>\n");
		    fwrite($outFile, "</category>\n");
	    }

	    fwrite($outFile, "</aiml>");
	    fclose($outFile);
	}
	
}