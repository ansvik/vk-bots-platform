<?php

class Controller_Bind_to_vk extends Controller
{
    
    function __construct()
    {
        $this->view = new View();
        $this->model = new Model_Bind_to_vk();
    }
    
    function action_index()
    {
        if ($_SESSION['mainbot'] == -1)
        {
            header('Location: error?num=2');
        }
        $data = $this->model->get_data();
        $this->view->generate('bind_to_vk_view.php', 'template_view.php', $data);
    }
    
    function action_get_code()
    {
        if (isset($_GET['code'])) {
            $this->model->get_code($_GET['code']);
        }
    }
    
    function action_check_token()
    {
        if (isset($_GET['link']) && isset($_GET['group'])){
            $_SESSION['group'] = $_GET['group'];
            $ini = parse_ini_file(get_settings_path());
            if (isset($ini[$_GET['group']])){
                $this->model->bind($ini[$_GET['group']], $_GET['group']);
            }
        header('Location: ' . $_GET['link']);
        }
    }
    
    function action_unbind()
    {
        if (!isset($_GET['group']))
        {
            return;
        }
        
        $ini = parse_ini_file(get_settings_path());
        
        $params = array(
            'group_id' => $_GET['group'],
            'access_token' => $ini[$_GET['group']],
            'v' => '5.80'
            );
            
        $params = http_build_query($params);
        
        $response = file_get_contents('https://api.vk.com/method/groups.getCallbackServers?' . $params);
        $response = json_decode($response, true);
        
        $id = null;
        
        foreach($response['response']['items'] as $item)
        {
            if ($item['title'] == 'Chatbot') 
            {
                $id =  $item['id'];
                break;
            }
        }
        
        if ($id == null) 
        {
            return;
        }
        
        $params = array(
            'group_id' => $_GET['group'],
            'server_id' => $id,
            'access_token' => $ini[$_GET['group']],
            'v' => '5.80'
            );
            
        $params = http_build_query($params);
        
        $response = file_get_contents('https://api.vk.com/method/groups.deleteCallbackServer?' . $params);
        
        unlink(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' . $_GET['group'] . '.php');
        
        header('Location: ok?code=2');
    }
    
}