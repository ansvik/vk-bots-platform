<?php

class Controller_Bot_change extends Controller
{
	
	function __construct()
	{
		$this->model = new Model_Bot_change();
		$this->view = new View();
	}
	
	function action_index()
	{
	    $data = $this->model->get_data();
		$this->view->generate('bot_change_view.php', 'template_view.php', $data);
	}
	
	function action_set_main()
	{
	    $_SESSION['mainbot'] = $_POST['bot']; 
        $ini = null;
        if (file_exists(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/settings.ini')){
            $ini = parse_ini_file(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/settings.ini');
            $ini['mainbot'] = $_SESSION['mainbot'];
            $outFile = WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/settings.ini';
            ini_write($ini, $outFile);
            echo "ok2";
        }
        else {
            $outFile = fopen(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/settings.ini',"w");
            fwrite($outFile, 'mainbot = ' . $_POST['bot']);
            echo "ok";
        }
	}
	
	function action_add()
	{
	    if (file_exists(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' . $_REQUEST['bot'] . '.aiml')) {
            echo 'Этот бот уже существует.';
            return;
        }
        if ($_SESSION['mainbot'] == -1) {
             $_SESSION['mainbot'] = $_REQUEST['bot'] . '.aiml';
        }
        $template = file_get_contents(WEBSITE_PATH . '/application/users/template.aiml');
        $file = fopen(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' . $_REQUEST['bot'] . '.aiml', 'w');
        fwrite($file, $template);
        echo 'ok';
	}
	
	function action_delete()
	{
	    if ($_SESSION['mainbot'] == $_REQUEST['bot']) {
            $_SESSION['mainbot'] = -1;
        }
        unlink(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' . $_REQUEST['bot']);
        echo 'ok';
	}
}