<?php

class Controller_Conversation extends Controller
{
	
	function action_index()
	{
	    if ($_SESSION['mainbot'] == -1)
        {
            header('Location: error?num=2');
        }
		$this->view->generate('conversation_view.php', 'template_view.php');
	}
	
	function action_get_answer()
	{
	    if (isset($_GET['question'])) {
            $x = parse(WEBSITE_PATH . '/application/users/' . $_SESSION['user']['id'] . '/' . $_SESSION['mainbot'], $_GET['question']);
            echo $x;
        }
	}
}
