var timer = function(time, func_while, func_end)
        {
            var x = time;
            
            var z = window.setInterval(function() {
                func_while();    
                if (x == 0) 
                {
                    func_end();
                    clearInterval(z);
                }
                x--;
            }, 1000);
        }
        
var notify = function(text)
    {
        timer(1, 
        function(){ 
            var label = document.getElementById('notify');
            label.style.display = 'inline';
            label.innerHTML = text;
        },
        function(){
            var label = document.getElementById('notify');
            label.style.display = 'none';
            label.innerHTML = '';
        });
    }
        
        
        
        